package com.gitlab.nurkholik.firebaseadmin;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

public class FirebaseAdmin {
	
	/**
	 * @param configPathFile	> Absolute path of json config firebase admin key
	 * @param databaseUrl		> url firebase cloud database
	 * @throws IOException
	 */
	public FirebaseAdmin(String configPathFile, String databaseUrl) throws IOException {
		System.out.println("Initialize firebase admin ...");
		FileInputStream serviceAccount = new FileInputStream(configPathFile);
		FirebaseOptions options = new FirebaseOptions.Builder()
				.setCredentials(GoogleCredentials.fromStream(serviceAccount))
				.setDatabaseUrl(databaseUrl)
				.build();
		if (!FirebaseApp.getApps().isEmpty()) {
			FirebaseApp.getApps().forEach(e -> {
				System.out.println("Delete instance : " + e.getName());
				e.delete();
			});
		}
		FirebaseApp.initializeApp(options);
		System.out.println("Firebase admin initialized successfully.");
	}
	
	public void sendNotifTopic(String title, String message, String topic) throws FirebaseMessagingException {
		send(title, message, null, topic, null);
	}
	
	public void sendNotifTopic(String title, String message, String topic, Map<String, String> data) throws FirebaseMessagingException {
		send(title, message, null, topic, data);
	}
	
	public void sendNotifUser(String title, String message, @NotNull String token) throws FirebaseMessagingException {
		send(title, message, token, null, null);
	}
	
	public void sendNotifUser(String title, String message, String token, Map<String, String> data) throws FirebaseMessagingException {
		send(title, message, token, null, data);
	}
	
	private void send(String title, String message, String token, String topic, Map<String, String> data) throws FirebaseMessagingException {
		com.google.firebase.messaging.Message.Builder gcmMessage = Message.builder();
		gcmMessage.setNotification(new Notification(title, message));
		
		System.out.println("Sending notification ...");
		if (token != null) gcmMessage.setToken(token);
		if (topic != null) gcmMessage.setTopic(topic);
		if (data != null) gcmMessage.putAllData(data);
		
		FirebaseMessaging.getInstance().send(gcmMessage.build());
		System.out.println("Send notification success.");
	}
	
	public static void main(String[] args) throws IOException, FirebaseMessagingException {
		FirebaseAdmin admin = new FirebaseAdmin(
			ClassLoader.getSystemResource("push-me.json").getPath(),
			"https://push-me-247605.firebaseio.com"
		);
		
		admin.sendNotifUser (
			"From Admin", 
			"Hallo, welcome to this world ... ",
			"eaXYUWfam_0:APA91bF-bha4IvRdds1AaCkkrixGNXeXX5vlMafErmxpveTyXKxjbzAupWR1O8hx4l0gcLitK7YBgY5hrHw_fkI-xYBbVGBhQZQpHswpZ0p4ofP2Uy_qJaDp5i-5yS9-8KrES-SuUtsy"
		);
	}
	
}
